Floating Windows Module
================================================================================

DESCRIPTION:
--------------------------------------------------------------------------------
This module provides you both an API as an User Interface to create draggable 
inline windows which can be used to display messages, images, load drupal forms 
and lots of other things.

INSTALLATION:
--------------------------------------------------------------------------------
1. Download and the required jquery_plugin module 
     (http://drupal.org/project/jquery_plugin).

2. Install the jquery_plugin module by navigating to:
     admin/build/modules

3. Download the jQuery plugin toJson and save it as jquery.json.min.js inside 
   the jquery_plugin module folder.
     (http://jollytoad.googlepages.com/json.js)

4. Download the jQuery plugin cssPNGFix and save it as jquery.csspngfix.min.js 
   inside the jquery_plugin module folder.
     (http://plugins.jquery.com/project/cssPNGFix)

5. Download the jQuery plugin jqDnR and save it as jquery.jqDnR.min.js inside
   the jquery_plugin module folder.
     (http://dev.iceburg.net/jquery/jqDnR/)

6. Download the jQuery plugin disableTextSelect and save it as 
   jquery.text_selection.min.js inside the jquery_plugin module folder.
     (http://plugins.jquery.com/project/disable_text_select)

7. Download the jQuery plugin swapDepths and save it as 
   jquery.swapdepths.min.js inside the jquery_plugin module folder.
     (http://plugins.jquery.com/project/swapdepths)

8. Install the float_window module by navigating to:
     admin/build/modules

9. Configure and create floating windows by navigating to:
     admin/build/float_window


USAGE:
--------------------------------------------------------------------------------
Read the help section by navigating to admin/help/float_window. Here you'll 
find a complete manual on how the module can be used.