
/**
 * This function helps developers to copy te code of a floating window's settings to the clipboard
 * That code then can be used in for eample an hook_enable of a module 
 */
function floatWinGetCode(lid) {
  var win_id = lid.substring(0, lid.lastIndexOf('-code'));
  $.ajax({
    url:(typeof(Drupal.settings.base_path) == 'object') ? Drupal.settings.base_path[0] + 'float_window_response' : Drupal.settings.base_path + 'float_window_response',
    data:{get_window_properies:win_id},
    dataType:'json',
    type:'POST',
    success:function(json) {
      $.copyToClipboard(json.response);
      if(window.trace) {
        trace('The code is copied to your clipboard!');
      } else {
        alert('The code is copied to your clipboard!');
      };
    }
  });
};

/**
 * 
 */
$(function() {
  $('.floatwin_code').click(function() { 
    floatWinGetCode($(this).attr('id'));
    return false;
  });
});
